'''
word frequency bot
'''
import os
import string


#os.chdir("C:\Users\krer\Documents\Dev\Chatbot")


def read_input(filename):
    '''
    reads in input and returns it as a list of words
    cleans all words that contain non alphabetical characters
    '''
    words = []
    lst = ''
    sentences = []
    file = open(filename, 'r')

    for i in file.read():
        if i != '.':
            lst+=i
        else:
            lst+='\n'

    for i in lst.split('\n'):
        if len(i)!=0:
            inner = []
            for word in i.split():
                inner.append(clean_word(word))
            sentences.append(inner)

    return sentences



def clean_word(word):
    '''
    removes non alphabetic characters from a word
    and puts the word in lower case
    '''
    if(word.isalpha()):
        return word.lower()
    

    wlist = list(word)

    #print(wlist)
    word = ''
    for i in range(0,len(wlist)):
        if wlist[i].isalpha():
            word+=wlist[i]    
    return word.lower()


def gen_dictionary(sentences):
    '''
    generates a dictionary in format {word : (count, {pre dic}, {post dic})}
    pre - a list of words appeared after the word
    post - a list of words that have appeared before the word

    NEED TO:
    -> add the top word frequencies : DONE
    -> add checker whether the sentence is single word

    '''
    word_freqs = {}

    for sen in sentences:
        for i in range(1, len(sen)-1):
            curr_word = sen[i]
            prev_word = sen[i-1]
            next_word = sen[i+1]

            if curr_word in word_freqs:
                word_freqs[curr_word][0] +=1

                if prev_word in word_freqs[curr_word][1]:
                    word_freqs[curr_word][1][prev_word] +=1
                else:
                    word_freqs[curr_word][1][prev_word] = 1
                

                if next_word in word_freqs[curr_word][2]:
                    word_freqs[curr_word][2][next_word] +=1
                else:
                    word_freqs[curr_word][2][next_word] = 1
                
            else:
                word_freqs[curr_word] = [1, {}, {}, get_type(curr_word)]

                if prev_word in word_freqs[curr_word][1]:
                    word_freqs[curr_word][1][prev_word] +=1
                else:
                    word_freqs[curr_word][1][prev_word] = 1
                

                if next_word in word_freqs[curr_word][2]:
                    word_freqs[curr_word][2][next_word] +=1
                else:
                    word_freqs[curr_word][2][next_word] = 1
            
    return word_freqs
            


def get_type(word):
    '''
    this will take a word and lookup in my own dictionary database

    '''
    return True





def main():
    #print(clean_word(":::abc:"))
    word_list = read_input("speech1.txt")
    print(word_list)
    
    
    
    dic = gen_dictionary(word_list)
    for i in dic:
        print(i)
        print(dic[i])
    
    #print(dic)


    



main()

